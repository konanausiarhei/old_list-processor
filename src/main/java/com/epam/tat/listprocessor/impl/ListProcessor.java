package com.epam.tat.listprocessor.impl;

import com.epam.tat.listprocessor.IListProcessor;
import com.epam.tat.listprocessor.exception.ListProcessorException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Function Description:
 * Complete the functions below. All methods must work with list of String.
 * <p>
 * In case of incorrect input values or inability to perform an action, the method should throw an appropriate
 * exception.
 */
public class ListProcessor implements IListProcessor {
    private static final Pattern VOWELS_PATTERN =
            Pattern.compile("[aeiou]", Pattern.CASE_INSENSITIVE);
    private static final Pattern CONSONANTS_PATTERN =
            Pattern.compile("[^\\s\\daeiou]", Pattern.CASE_INSENSITIVE);

    /**
     * Find the second by length string in a list.
     * <p>
     * Ex.:
     * From list:
     * {"a", "aa", "aaaaa", "aaaa", "aaa"}
     * will be return "aaaa"
     *
     * @param list - input data
     * @return second by length string in the input list
     */
    @Override
    public String getSecondStringByLength(List<String> list) {

        throwIfIsEmptyOrNull(list);

        TreeMap<Integer, LinkedList<String>> mappedByLength = list.stream()
                .collect(Collectors.groupingBy(
                        String::length, TreeMap::new, Collectors.toCollection(LinkedList::new)));

        if (mappedByLength.size() < 2) {
            throw new ListProcessorException();
        }

        mappedByLength.pollLastEntry();

        return mappedByLength.pollLastEntry().getValue().getFirst();
    }

    /**
     * Sort list by string length.
     * <p>
     * Ex.:
     * From list:
     * {"a", "aa", "aaA", "aAa", "aaa", "Aa"}
     * will be return
     * {"a", "Aa", "aa", "aAa", "aaA", "aaa"}
     *
     * @param list - input data
     * @return sort list by string length
     */
    @Override
    public List<String> getSortedListByLength(List<String> list) {

        throwIfIsEmptyOrNull(list);

        return list.stream().sorted(Comparator.comparing(String::length))
                .collect(Collectors.toList());
    }

    /**
     * Sort list or array by count of vowels in string.
     * If the number of vowels in several words is the same, the order is alphabetical.
     * <p>
     * Ex.:
     * From list:
     * {"Puma", "Nike", "Timberland", "Adidas"}
     * will be return
     * {"Nike", "Puma", "Adidas", "Timberland"}
     *
     * @param list - input data
     * @return sort list by string length
     */
    @Override
    public List<String> getSortedListByCountOfVowels(List<String> list) {

        throwIfIsEmptyOrNull(list);

        return list.stream().sorted(
                Comparator.comparing(this::numberOfVowelsLetters)
                        .thenComparing(String::compareTo)
                        .thenComparing(String::length))
                .collect(Collectors.toList());
    }

    private long numberOfVowelsLetters(String letters) {

        long number = VOWELS_PATTERN.matcher(letters).results().count();

        if (number == 0) {
            throw new ListProcessorException();
        }

        return number;
    }

    /**
     * Sort list or array by count of consonants in string.
     * If the number of consonants in several words is the same, the order is alphabetical.
     * <p>
     * Ex.:
     * From list:
     * {"Puma", "Nike", "Timberland", "Adidas"}
     * will be return
     * {"Nike", "Puma", "Adidas", "Timberland"}
     *
     * @param list - input data
     * @return sort list by string length
     */
    @Override
    public List<String> getSortedListByCountOfConsonants(List<String> list) {

        throwIfIsEmptyOrNull(list);

        return list.stream().sorted(
                Comparator.comparing(this::numberOfConsonantsLetters)
                        .thenComparing(String::compareTo))
                .collect(Collectors.toList());
    }

    private long numberOfConsonantsLetters(String letters) {

        long number = CONSONANTS_PATTERN.matcher(letters).results().count();

        if (number == 0) {
            throw new ListProcessorException();
        }

        return number;
    }

    /**
     * Change by places first and last symbols in each second string of list.
     * <p>
     * Ex.:
     * From list:
     * {"Puma", "Nike", "Timberland", "Adidas"}
     * will be return
     * {"Puma", "eikN", "Timberland", "sdidaA"}
     *
     * @param list - input data
     * @return sort list by string length
     */
    @Override
    public List<String> changeByPlacesFirstAndLastSymbolsInEachSecondStringOfList(List<String> list) {

        throwIfIsEmptyOrNull(list);

        if (list.size() < 2) {
            throw new ListProcessorException();
        }

        List<String> modifiedStrings = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            String initialString = list.get(i);

            if (i % 2 == 1 && initialString.length() >= 2) {
                char[] chars = initialString.toCharArray();
                char tmp = chars[0];

                chars[0] = chars[chars.length - 1];
                chars[chars.length - 1] = tmp;
                modifiedStrings.add(new String(chars));
            } else {
                modifiedStrings.add(initialString);
            }

        }

        return modifiedStrings;
    }

    /**
     * Revert strings of list.
     * <p>
     * Ex.:
     * From list:
     * {"Puma", "Nike", "Timberland", "Adidas"}
     * will be return
     * {"amuP", "ekiN", "dnalrebmiT", "sadidA"}
     *
     * @param list - input data
     * @return sort list by string length
     */
    @Override
    public List<String> reverseStringsOfList(List<String> list) {

        throwIfIsEmptyOrNull(list);

        return list.stream()
                .map(StringBuilder::new)
                .map(StringBuilder::reverse)
                .map(StringBuilder::toString)
                .collect(Collectors.toList());
    }

    private void throwIfIsEmptyOrNull(List<?> list) {

        if (list == null || list.isEmpty()) {
            throw new ListProcessorException();
        }

    }

}
